/* We pretend everything is graphed on some interval symmetric about the origin;
 * this is our view variable. box is a slightly larger interval, which we use
 * for convolutions. */

const w = 1300; // dimensions of graph figure
const h = 500;
const padding = 50;
const totHt = h + 2.2 * padding; // dimensions of total figure
const totWd = w + padding;
const pts = 1000; // number of points to plot in viewing window
const oneYear = (new Date(1971, 0, 0)).getTime() -
  (new Date(1970, 0, 0)).getTime();
const oneHour = (new Date(1970, 0, 0, 1)).getTime() -
  (new Date(1970, 0, 0)).getTime();
const maxData = 70; // maximum number of individual points to show
const pointRadius = 7;
const minSmoothness = oneHour / (3600 * oneYear);
const maxSmoothness = 0.5;
const headers =
  {'Origin': 'https://github.com/benfl/wikipedia-edit-visualization',
'Content-Type': 'application/json; charset=UTF-8'
  };
const apiUrl = 'https://en.wikipedia.org/w/api.php?action=query&' +
  'prop=revisions&rvprop=timestamp|user|parsedcomment|size|tags|ids' +
  '&rvlimit=max&rvdir=newer&redirects&origin=*&format=json&titles=';
const startDivWidth = 400;
const startDivHeight = 260;
const loadingDivWidth = 200;
const loadingDivHeight = 100;
let article_id;
let smoothness;
let eps = 1e-10;
let view;
let box;
let maxBounds;
let radius;
let pointForce;
let freqActive = true;
let sizeActive = true;
let title = '';
let dataset;

const timeFormat = d3.utcFormat('%H:%M, %-e %b %Y');
const dayFormat = d3.utcFormat('%-e %b %Y');
const timeParse = d3.utcParse('%Y-%m-%dT%H:%M:%SZ');

const xScale = d3.scaleTime()
  .range([padding, w - padding]);

const viewScale = d3.scaleLinear()
  .domain([0, pts]);

const realIntervalScale = d3.scaleLinear()
  .domain([padding, w - padding])
  .clamp(true);

const realScale = d3.scaleLinear()
  .domain([padding, w - padding])
  .range([padding, w - padding])
  .clamp(true);

d3.select("body")
  .on("click", function() {
    const avoidDiv = d3.selectAll("div#tooltip *")
      .filter((d, i, nodes) => nodes[i] == d3.event.target).empty();
    const avoidCirc = d3.selectAll("#infoCircle")
      .filter((d, i, nodes) => nodes[i] == d3.event.target).empty();
    if (avoidDiv && avoidCirc) {
      d3.select("div#tooltip").classed("hidden", true);
    }
  });

const svg = d3.select("body")
  .append("svg")
  .attr("width", totWd)
  .attr("height", totHt)
  .attr("position", "relative");

svg.append("rect")
  .attr("id", "clickOverlay")
  .attr("x", padding)
  .attr("y", padding)
  .attr("width", w - 2 * padding)
  .attr("height", h - 2 * padding)

svg.append("rect")
  .attr("id", "dragRectangle")
  .attr("y", padding)
  .attr("height", h - 2 * padding);

svg.append("rect")
  .attr("id", "freqRect")
  .attr("y", 2.25 * padding)
  .attr("x", w - 1.75 * padding)
  .attr("width", padding / 2)
  .attr("height", padding / 2)
  .append("title")
  .text("Edit frequency");

svg.append("rect")
  .attr("id", "sizeRect")
  .attr("y", 3 * padding)
  .attr("x", w - 1.75 * padding)
  .attr("width", padding / 2)
  .attr("height", padding / 2)
  .append("title")
  .text("Article size");

const zoomBox = svg.append("g")
  .attr("id", "zoomOut")
  .classed("hidden", true);

zoomBox.append("rect")
  .attr("y", padding)
  .attr("x", w - 2 * padding)
  .attr("rx", padding / 4)
  .attr("ry", padding / 4)
  .attr("width", padding)
  .attr("height", padding)
  .attr("fill", "#00ff00")
  .attr("opacity", 0.2);

zoomBox.append("circle")
  .attr("cx", w - 1.5 * padding)
  .attr("cy", 1.5 * padding)
  .attr("r", padding / 4)
  .attr("stroke", "black")
  .attr("fill", "none")
  .attr("stroke-width", "3px");

zoomBox.append("line")
  .attr("x1", w - 1.625 * padding)
  .attr("x2", w - 1.375 * padding)
  .attr("y1", 1.5 * padding)
  .attr("y2", 1.5 * padding)
  .attr("stroke", "black")
  .attr("stroke-width", "3px");

zoomBox.append("line")
  .attr("x1", w - 1.323 * padding)
  .attr("y1", 1.677 * padding)
  .attr("x2", w - 1.123 * padding)
  .attr("y2", 1.877 * padding)
  .attr("stroke", "black")
  .attr("stroke-width", "3px");

const ptCountGp = svg.append("g");

const ptCountBox = ptCountGp.append("rect")
  .attr("x", 1.5 * padding)
  .attr("y",  1.6 * padding)
  .attr("height",  padding / 2)
  .attr("fill", "#ffffff00");

ptCountBox.append("title")
  .text("Total number of edits in current view");

ptCountGp.append("text")
  .attr("x", 1.5 * padding)
  .attr("y", 2 * padding)
  .attr("id", "pointCount")
  .attr("text-anchor", "start");

const titleGp = svg.append("g");

const titleBox = titleGp.append("rect")
  .attr("id", "titleBox")
  .attr("x", 1.5 * padding)
  .attr("y",  padding)
  .attr("height",  padding / 2)
  .attr("fill", "#ffffff00")
  .on("dblclick", reInit); 

titleGp.append("text")
  .attr("x", 1.5 * padding)
  .attr("y", 1.4 * padding)
  .attr("id", "titleText")
  .attr("text-anchor", "start");


titleBox.append("title")
  .text("Current article (double click to change)");
//w - 2.5 * padding)
/*
ptCountBox.append("title")

ptCountGp.append("text")
  .attr("x", 1.5 * padding)
  .attr("y", 1.5 * padding)
  .attr("id", "pointCount")
  .attr("text-anchor", "start");
  */
svg.append("path")
  .attr("id", "size")
  .style("stroke", "orange")
//.attr("class", "line");


svg.append("path")
  .attr("id", "freq")
  .style("stroke", "blue")
// .attr("class", "line");

const infoPoints = svg.append("g")
  .attr("transform", "translate(500, 500)");


// scale from dates to an interval, where a year has length 1
const timeScale = d3.scaleTime(); 


// slider 
const smoothHandleScale = d3.scaleLinear()
  .domain([padding, w - padding])
  .range([padding, w - padding])
  .clamp(true);


const smoothScale = d3.scalePow() 
  .exponent(4)
  .domain([padding, w - padding])
  .range([minSmoothness, maxSmoothness])
  .clamp(true);

const smoothSlider = svg.append("g")
  .attr("class", "slider");

smoothSlider.append("line")
  .attr("class", "slider")
  .attr("id", "track")
  .attr("x1", padding)
  .attr("x2", w - padding)
  .attr("y1", padding / 2)
  .attr("y2", padding / 2);

smoothSlider.append("circle")
  .attr("id", "smoothHandle")
  .attr("r", 9)
  .attr("cx", -10000)
  .attr("cy", padding / 2);
//  .attr("cx", smoothScale.invert(smoothness));

smoothSlider.append("line")
  .attr("class", "slider")
  .attr("id", "overlay")
  .attr("stroke-width", padding)
  .attr("x1", 0)
  .attr("x2", w)
  .attr("y1", padding / 2)
  .attr("y2", padding / 2)
  .append("title")
  .html("←: more detail, sees short term effects \n→: smoother, sees long term effects");

svg.append("g").attr("class", "axis")
  .attr("id", "xAxis")
  .attr("transform", "translate(0, " + (h - padding) + ")");

svg.append("rect")
  .attr("height", h - 2 * padding)
  .attr("width", padding)
  .attr("x", 0)
  .attr("y", padding)
  .attr("opacity", 0)
  .append("title")
  .text("Relative frequency of edits");

svg.append("g").attr("class", "axis")
  .attr("id", "freqAxis")
  .attr("transform", "translate(" + padding + ", 0)");

svg.append("rect")
  .attr("height", h - 2 * padding)
  .attr("width", padding)
  .attr("x", w - padding)
  .attr("y", padding)
  .attr("opacity", 0)
  .append("title")
  .text("Size of article (bytes)");

svg.append("g").attr("class", "axis")
  .attr("id", "sizeAxis")
  .attr("transform", "translate(" + (w - padding) + ", 0)");

d3.select("div#start")
  .style("vertical-align", "middle")
  .style("left", (w / 2 - startDivWidth / 2) + "px")
  .style("top", (h / 2 - startDivHeight / 2) + "px");

d3.select("div#loading")
  .style("left", (w / 2 - loadingDivWidth / 2) + "px")
  .style("top", (h / 2 - loadingDivHeight / 2) + "px")
  .classed("hidden", true);
/*function processRow(d) {
  return {
    number: +d.number,
    size: +d.size,
    dSize: +d.dSize,
    time: timeParse(d.timestamp),
    user: d.user,
    comment: d.parsedcomment,
    url: (d.parentid === "0") ? "https://en.wikipedia.org/w/index.php?&oldid=" + d.revid
    : "https://en.wikipedia.org/w/index.php?&diff=" + d.revid + "&oldid=" + d.parentid
  };
} */

function mirroredTime(dataset, i) {
  // Return time at position i in dataset. If i is out of bounds,
  // "mirror" the data across the boundaries and return the mirrored value.

  
  if (i >= 0 && i < dataset.length)
    return dataset[i].time;
  else if (i < 0) {
    return new Date(2 * dataset[0].time.getTime() -
      dataset[-i].time.getTime());
  }
  else
    return new Date(2 * dataset[dataset.length - 1].time.getTime() -
      dataset[2 * dataset.length - i - 2].time.getTime());
}

function smthData(dataset, factor) {
  const out = [];
  const points = [];

  const histLength = Math.min(3 * pts - 2,
    pts + 2 * Math.round(pts * radius / (view[1] - view[0]))) 

  const hist = new Array(histLength).fill(0); // histogram of number of edits
  const gaussValues = new Array(histLength).fill(0);
  const sizes = new Array(histLength).fill(0);

  const timeHistScale = d3.scaleTime()
    .domain(box.map(timeScale.invert))
    .range([0, hist.length - 1]);

  let sizeIndex = 0;
  let prevSize = 0;

  for (let i = -dataset.length + 1;
    timeScale(mirroredTime(dataset, i)) <= box[1] && i < 2 * dataset.length - 2;
    i++) {

    const t = mirroredTime(dataset, i)
    if (timeScale(t) >= box[0]) {
      hist[Math.round(timeHistScale(t))] ++;

      for(; sizeIndex < timeHistScale(t); sizeIndex++) {
        sizes[sizeIndex] = prevSize;
      }

    }
    if (timeScale(t) >= view[0] && timeScale(t) <= view[1]) {
      points.push(dataset[i]);
    }
    if (i < 0) {
      prevSize = dataset[-i].size;
    } 
    else if (i >= dataset.length) {
      prevSize = dataset[2 * dataset.length - 2 - i].size;
    }
    else {
      prevSize = dataset[i].size;
    }

  }
  for(; sizeIndex < sizes.length; sizeIndex++) {
    sizes[sizeIndex] = prevSize;
  }

  let gaussSum = 0;
  for (let i = 0; i < histLength / 2; i++) {
    dist = i * (view[1] - view[0]) / pts;
    const gau = Math.exp(- (dist ** 2) / (2 * factor ** 2));
    gaussValues[i] = gau;
    gaussValues[histLength - i - 1] = gau
    gaussSum += 2 * gau;
  }

  const convFreq = new Array(histLength);
  convolveReal(hist, gaussValues, convFreq);

  const convSize = new Array(histLength);
  convolveReal(sizes, gaussValues, convSize);

  for (let i = 0; i < pts; i ++) {
    currTime = timeScale.invert(viewScale(i));
    out.push({time: currTime,
      freq: convFreq[Math.round(timeHistScale(currTime))],
      size: convSize[Math.round(timeHistScale(currTime))] / gaussSum,
      x: xScale(currTime),
      y: h + 1.3 * padding});
  }

  return {values: out, // smoothed data
    points: points}; // points inside viewing window
}

function plotDeriv(sm, svg, trans=true) {
  const yScale = d3.scaleLinear()
    .domain([0, Math.max(eps, d3.max(sm, d => d.freq))]) //avoid floating pt noise
    .range([h - padding, padding]);

  const yAxisScale = d3.scaleLinear()
    .domain([0, 1])
    .range([h - padding, padding]);

  //xScale.domain([sm[0].time, sm[sm.length - 1].time]);

  const line = d3.line()
    .x(d => xScale(d.time))
    .y(d => yScale(d.freq));

  const xAxis = d3.axisBottom(xScale).tickFormat(dateFormat);
  const yAxis = d3.axisLeft(yAxisScale);

  if (trans) {
  svg.select("path#freq")
    .datum(sm)
    .transition()
    .attr("d", line);
  }
  else {
  svg.select("path#freq")
    .datum(sm)
    .attr("d", line);
  }

  svg.select("#xAxis").call(xAxis)
    .selectAll("text")
    .style("text-anchor", "end")
    .attr("transform", "rotate(-45)")
  svg.select("#freqAxis").call(yAxis);

}

function plotPoints(points, svg) {
  svg.selectAll("#infoCircle")
    .remove();

  d3.select("text#pointCount").text(points.length);
  ptCountBox.attr("width", d3.select("text#pointCount").node().getBBox().width)


  if (points.length < maxData) {
    const nodes = svg.selectAll("circle#infoCircle")
      .data(points, d => d.time)
      .enter()
      .append("circle")
      .attr("id", "infoCircle")
      .attr("cy", d => d.y)
      .attr("y", d => d.y)
      .attr("r", pointRadius)
      .attr("fill", "yellow")
      .attr("x", d => d.x)
      .attr("cx", d => d.x)
      .on("click", function(d) {
        const x = (+ d3.select(this).attr("cx") + pointRadius);
        console.log(x);
        const tooltip = d3.select("div#tooltip")
          .style("top", (d3.select(this).attr("cy"))
            + "px")
          .style("transform", "translateY(-100%)")
          .style("left", x + "px")
          .classed("hidden", false);
        if (x > w / 2) {
          tooltip.style("transform", "translateY(-100%) translateX(-100%)")
        }


        tooltip.select("span#user")
          .text(d.user);
        tooltip.select("span#date")
          .text(timeFormat(d.time));
        tooltip.select("span#comment")
          .html(d.comment);
        tooltip.select("span#size")
          .text(d.size + " Bytes");
        tooltip.select("a#link")
          .attr("href", d.url);
        if (d.dSize >= 0) {
          tooltip.select("span#dSize")
            .text("(+" + d.dSize + ")")
            .style("color", "green");
        }
        else {
          tooltip.select("span#dSize")
            .text("(" + d.dSize + ")")
            .style("color", "red");
        }
        if (d.comment == "") {
          tooltip.select("span#commentLabel").classed("hidden", true);
        }
        else {
          tooltip.select("span#commentLabel").classed("hidden", false);
        }
      })
      .on("mouseover", function(d) {
        d3.select(this).attr("fill", "red");
      })
      .on("mouseout", function(d) {
        d3.select(this).attr("fill", "yellow");
      });


    // avoid overlaps
    pointForce = d3.forceSimulation(points)
      .force("x", d3.forceX(d => xScale(d.time)).strength(5))
      .force("y", d3.forceY(d => h + 1.3 * padding).strength(5))
      .force("collision", d3.forceCollide(1.2 * pointRadius).strength(0.2))
      .alphaDecay(0.2)
      .on("tick", function() {
        nodes.attr("cx", d => d.x);
        nodes.attr("cy", d => d.y);
      })
      .on("end", stopForce);

    function stopForce() {
      if (pointForce)
        pointForce.stop();
    }
  }
}

function plotSize(dataset, svg, trans=true) {
  const yScale = d3.scaleLinear()
    .domain([0, d3.max(dataset, d => d.size)])
    .range([h - padding, padding]);

  const line = d3.line()
    .x(d => xScale(d.time))
    .y(d => yScale(d.size));

  const yAxis = d3.axisRight(yScale);
  svg.select("#sizeAxis").call(yAxis);

  if (trans) {
  svg.select("path#size")
    .datum(dataset)
    .transition()
    .attr("d", line);
  }
  else {
  svg.select("path#size")
    .datum(dataset)
    .attr("d", line);
  }

}

function plot(dataset, svg, trans=true) {
  derivData = smthData(dataset, smoothness);
  if (sizeActive) {
    d3.select("path#size").classed("hidden", false)
    d3.select("#sizeAxis").classed("hidden", false)
    plotSize(derivData.values, svg, trans);
  }
  else {
    d3.select("path#size").classed("hidden", true)
    d3.select("#sizeAxis").classed("hidden", true)
  }
    
  plotPoints(derivData.points, svg);

  if (freqActive) {
    d3.select("path#freq").classed("hidden", false)
    d3.select("#freqAxis").classed("hidden", false)
    plotDeriv(derivData.values, svg, trans);
  }
  else {
    d3.select("path#freq").classed("hidden", true)
    d3.select("#freqAxis").classed("hidden", true)
  }
}

function resize(x1, x2, midpt = (x1 + x2) / 2,
  r = Math.max(oneHour / oneYear, Math.abs(x2 - x1) / 2)) {

  x1 = Math.max(maxBounds[0], midpt - r);
  x2 = Math.min(maxBounds[1], midpt + r);

  if (view) {
    smoothness = smoothness * (x2 - x1) / (view[1] - view[0]);
  }
  else {
    smoothness = (x2 - x1) / 300;
  }
  if (smoothness < minSmoothness) {
    smoothness = minSmoothness;
  }
  else if (smoothness > maxSmoothness){
    smoothness = maxSmoothness;
  }
  d3.select("#smoothHandle").attr("cx", smoothScale.invert(smoothness));

  view = [x1, x2];
  if (x1 == maxBounds[0] && x2 == maxBounds[1]) {
    zoomBox.classed("hidden", true);
  }
  else {
    zoomBox.classed("hidden", false);
  }
  updateWindow();
}

function updateWindow() {
  xScale.domain([timeScale.invert(view[0]), timeScale.invert(view[1])])

  radius = Math.max(view[1] - view[0],
    10 * smoothness) //TODO magic number

  box = [view[0] - radius, view[1] + radius];
  viewScale.range(view);
  realIntervalScale.range(view);
}

function run(url, add='') {
  let cont;
  d3.json(url + add, headers).then(function(data) {
    const pages = data.query.pages; // improve this
    console.log(data);
    if (pages['-1']) {
      d3.select("span#notFound").text("Page not found");
      reInit();
    }
    else if(data.continue) {
      cont = data.continue.rvcontinue;
      dataset = dataset.concat(pages[Object.keys(pages)[0]].revisions);
      d3.select("span#amtLoaded")
        .text(dayFormat(timeParse(dataset[dataset.length - 1].timestamp)));
      run(url, '&rvcontinue=' + cont);
    }
    else {
      dataset = dataset.concat(pages[Object.keys(pages)[0]].revisions);
      title = pages[Object.keys(pages)[0]].title;
      d3.select("text#titleText").text(title);
      d3.select("rect#titleBox")
        .attr("width", d3.select("text#titleText").node().getBBox().width)
      dataset = processData(dataset);
      d3.select("div#loading").classed("hidden", true);
      d3.select("span#amtLoaded").text("");
      visualize(dataset);
    }
  });
}

function processData(dataset) {
  const out = [];
  let prevSize = 0;
  const baseUrl = 'https://en.wikipedia.org/w';
  for (const d of dataset) {
    const dSize = d.size - prevSize;
    prevSize = d.size;
    out.push(
      {
        comment: d.parsedcomment ?
        d.parsedcomment.replace('href="/w', 'href="' + baseUrl) : '',
        size: d.size,
        dSize: dSize,
        user: d.user,
        time: timeParse(d.timestamp),
        url: (d.parentid === "0") ? "https://en.wikipedia.org/w/index.php?&oldid=" + d.revid
        : "https://en.wikipedia.org/w/index.php?&diff=" + d.revid + "&oldid=" + d.parentid
      }
    );
  }
  return out;
}

function visualize(dataset) {
  const timeLength = dataset[dataset.length - 1].time.getTime() -
    dataset[0].time.getTime();
  const timeRange = timeLength / (2 * oneYear);

  timeScale.domain([dataset[0].time, dataset[dataset.length - 1].time]);
  timeScale.range([-timeRange, timeRange]);

  maxBounds = [timeScale(dataset[0].time), 
    timeScale(dataset[dataset.length - 1].time)];

  resize(maxBounds[0], maxBounds[1]);

  plot(dataset, svg, trans=false);

  d3.select("line#overlay").call(d3.drag()
    .on("start drag", function() {
      d3.select("circle#smoothHandle").attr("cx", smoothHandleScale(d3.event.x));
    })
    .on("end", function() {
      d3.select("circle#smoothHandle").attr("cx", smoothHandleScale(d3.event.x));
      smoothness = smoothScale(d3.event.x);
      updateWindow();
      plot(dataset, svg);
      if (pointForce)
        pointForce.stop();
    }));
  //.on("start.interrupt", smoothSlider.interrupt)

  let x1 = padding, x2 = w - padding;

  d3.select("rect#clickOverlay").call(d3.drag()
    .on("start", function() {
      x1 = d3.event.x
      d3.select("rect#dragRectangle")
        .attr("x", realScale(d3.event.x))
        .attr("fill", "#aa00aa")
        .attr("opacity", 0.3);
    })
    .on("drag", function() {
      x2 = d3.event.x;
      d3.select("rect#dragRectangle")
        .attr("x", Math.min(realScale(x1), realScale(x2)))
        .attr("width", Math.abs(realScale(x1) - realScale(x2)));
    })
    .on("end", function() {
      x2 = d3.event.x;
      d3.select("rect#dragRectangle")
        .attr("x", -100000)
        .attr("width", 0); 

      if (Math.abs(x2 - x1) > 10) { // in case of accidental dragging
        resize(realIntervalScale(x1), realIntervalScale(x2));
        plot(dataset, svg, trans=false);
      }
    })
  );

  d3.select("g#zoomOut").call(d3.drag()
    .on("end", function() {
      resize(x1 = view[0] - (view[1] - view[0]) / 2,
        x2 = view[1] + (view[1] - view[0]) / 2)
      plot(dataset, svg, trans=false);
    }));

  d3.select("rect#freqRect")
    .on("click", function() {
      freqActive = !freqActive;
      d3.select(this).attr("opacity", freqActive ? 1 : 0.2);
      if(!freqActive && !sizeActive) {
        sizeActive = !sizeActive;
        d3.select("rect#sizeRect").attr("opacity", sizeActive ? 1 : 0.2);
      }
      plot(dataset, svg, trans=false);
    });

  d3.select("rect#sizeRect")
    .on("click", function() {
      sizeActive = !sizeActive;
      d3.select(this).attr("opacity", sizeActive ? 1 : 0.2);
      if(!freqActive && !sizeActive) {
        freqActive = !freqActive;
        d3.select("rect#freqRect").attr("opacity", freqActive ? 1 : 0.2);
      }
      plot(dataset, svg, trans=false);
    });
}

d3.select("button")
  .attr("onclick", "init()")

document.getElementById("artID").addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode === 13) {
    document.getElementById("button").click();
  }
}); 

function init() {
  d3.select("span#notFound").text("");
  article_id = d3.select("input#artID").property("value").split("|")[0];
  dataset = []
  run(apiUrl + article_id);
  d3.select("div#start").classed("hidden", true);
  d3.select("div#loading").classed("hidden", false);
}

function reInit() {
  d3.select("div#start").classed("hidden", false);
  d3.select("div#loading").classed("hidden", true);
  dataset = []
}
