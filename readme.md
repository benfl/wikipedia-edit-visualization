Some functions to visualize how frequently Wikipedia articles get edited as a function of time. Try it out [here](https://benfl.github.io/wikipedia-vis).

I am indebted to [Project Nayuki](https://www.nayuki.io/page/free-small-fft-in-multiple-languages) for their implementation of the fast Fourier transform in Javascript.
