const formatMinute = d3.utcFormat("%H:%M"),
  formatHour = d3.utcFormat("%-I %p %b %-e %Y"),
  formatDay = d3.utcFormat("%b %d %Y"),
  formatMonth = d3.utcFormat("%B %Y"),
  formatYear = d3.utcFormat("%Y");

function dateFormat(date) {
  return (d3.timeHour(date) < date ? formatMinute
    : d3.timeDay(date) < date ? formatHour
    : d3.timeMonth(date) < date ? formatDay
    : d3.timeYear(date) < date ? formatMonth
    : formatYear)(date);
}
